<?php

namespace App\Controller;

use App\Entity\Movie;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class MovieController extends AbstractController
{
    /**
     * @Route("/", name="movie_list")
     */
    public function index()
    {
        $movies = $this->getDoctrine()
            ->getRepository(Movie::class)
            ->findAll();
        return $this->render('movies/index.html.twig', ['movies' => $movies]);
    }

    /**
     * @Route("/movie/new", name="new_movie")
     */
    public function Create(Request $request) {
        $movie = new Movie();

        $form = $this->createFormBuilder($movie)
            ->add('title', TextType::class, array('attr' =>
            array('class' => 'form-control')))
            ->add('body', TextareaType::class, array('attr' =>
                array('class' => 'form-control')))
//            ->add('movieImage', FileType::class, array('label' => 'Movie image (PNG file)'))
            ->add('save', SubmitType::class, array(
                'label' => 'Create',
                'attr' => array('class' => 'btn btn-primary mt-3')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $movie = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($movie);
            $entityManager->flush();

            return $this->redirectToRoute('movie_list');
        }

        return $this->render('movies/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/movie/delete/{id}", name="delete_movie")
     */
    public function Delete($id) {
        $entityManager = $this->getDoctrine()->getManager();
        $movie = $entityManager->getRepository(Movie::class)->find($id);

        $entityManager->remove($movie);
        $entityManager->flush();

        return $this->redirectToRoute('movie_list');
    }

    /**
     * @Route("/movie/edit/{id}", name="edit_movie")
     */
    public function Update(Request $request, $id) {
        $movie = new Movie();

        $movie = $this->getDoctrine()
            ->getRepository(Movie::class)
            ->find($id);

        $form = $this->createFormBuilder($movie)
            ->add('title', TextType::class, array('attr' =>
                array('class' => 'form-control')))
            ->add('body', TextareaType::class, array('attr' =>
                array('class' => 'form-control')))
            ->add('save', SubmitType::class, array(
                'label' => 'Create',
                'attr' => array('class' => 'btn btn-primary mt-3')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('movie_list');
        }

        return $this->render('movies/edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/movie/{id}", name="movie_show")
     */
    public function showMovie($id) {
        $movie = $this->getDoctrine()
            ->getRepository(Movie::class)
            ->find($id);
        return $this->render('movies/show.html.twig', ['movie' => $movie]);
    }
}
